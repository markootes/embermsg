<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'pages.index', 'uses' => 'PagesController@index'));


Route::group(array('prefix' => 'api'), function()
{
	// Users routes
	Route::resource('users', 'UsersController');
	Route::resource('messages', 'MessagesController');
	Route::post('login', array('as' => 'login', 'uses' => 'UsersController@login'));
	Route::get('logout', array('as' => 'logout', 'uses' => 'UsersController@logout'));
	Route::get('user', array('as' => 'user', 'uses' => 'UsersController@user'));

	// Conversation routes
	Route::resource('conversations', 'ConversationsController');
	Route::resource('conversations.messages', 'MessagesController');

});