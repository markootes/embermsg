<script type="text/x-handlebars" id="user">
	<div class='header'>
		{{#if App.mobile}}
			<button {{action 'backToUsers'}} class="leftcontrol">Contacten</button>
		{{/if}}
		<h1>{{username}}</h1>
	</div>


	<div class="user">
		<h2>{{username}}</h2>
		<p>{{email}}</p>


		{{#if isMe}}
			<button>Logout</button>


			{{else}}

			{{#unless hasConversationWithMe}}
				<button {{action transitionToRoute 'conversations.new'}}>Nieuw gesprek</button>
			{{/unless}}

			{{#each conversations}}
				{{#if conversationWithMe}}
					{{#link-to 'conversation' id}}Ga naar gesprek{{/link-to}}
				{{/if}}
			{{/each}}
		{{/if}}


	</div>
</script>