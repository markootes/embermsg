<script type="text/x-handlebars" id="conversation">

	<div class='header'>

		{{#if App.mobile}}
			<button {{action 'backToConversations'}} class="leftcontrol">Terug</button>
		{{/if}}

		<h1>
			{{#each user in participants}}
				{{#unless user.isMe}}
					<strong>{{user.username}}</strong>
				{{/unless}}
			{{/each}}
		</h1>

		<button {{action 'reloadMessages'}} class="rightcontrol">Ververs</button>
	</div>

	{{view App.MessagesView content=messages}}



	<form {{action "createMessage" on="submit"}} class="messageform" >
		{{textarea value=message name='message'}}
		<button>Stuur</button>
	</form>
</script>
