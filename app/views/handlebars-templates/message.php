<script type="text/x-handlebars" data-template-name="message">

		<p class="body">{{view.content.body}}</p>
		<span class="timeago">{{view.timeago}}</span>
</script>