<script type="text/x-handlebars" data-template-name="login">
	<h1>Embermsg</h1>

	<form {{action "login" on="submit"}}>

		{{#if loginFailed}}
				<div class="alert alert-danger">Inlogcombinatie is niet juist</div>
		{{/if}}

		{{input value=email type="email" placeholder="e-mailadres"}}

		{{input value=password type="password" placeholder="wachtwoord"}}
		<button type="submit">Log in!</button>
	</form>

</script>