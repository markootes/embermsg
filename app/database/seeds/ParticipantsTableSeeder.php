<?php

class ParticipantsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('participants')->truncate();

		$participants = array(
			array(
				'user_id' => 1,
				'conversation_id' => 1,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			),
			array(
				'user_id' => 2,
				'conversation_id' => 1,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			),
			array(
				'user_id' => 2,
				'conversation_id' => 2,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			),
			array(
				'user_id' => 3,
				'conversation_id' => 2,
				'created_at' => new DateTime,
				'updated_at' => new DateTime
			)
		);

		// Uncomment the below to run the seeder
		DB::table('participants')->insert($participants);
	}

}
