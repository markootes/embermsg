<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->truncate();

        User::create(array(
            'username' => 'rickootes',
            'email' => 'rick.ootes@gmail.com',
            'password' => Hash::make('password')
        ));

        User::create(array(
            'username' => 'markootes',
            'email' => 'mark.ootes@gmail.com',
            'password' => Hash::make('password')
        ));

        User::create(array(
            'username' => 'carlodekker',
            'email' => 'carlodekker@gmail.com',
            'password' => Hash::make('password')
        ));
	}

}
