<?php

class ConversationsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('conversations')->truncate();

		Conversation::create(array(
            'user_id' => 1
        ));

        Conversation::create(array(
            'user_id' => 2
        ));

		// Uncomment the below to run the seeder
		// DB::table('conversations')->insert($conversations);
	}

}
