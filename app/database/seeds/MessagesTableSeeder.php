<?php

class MessagesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		DB::table('messages')->truncate();

		$messages = array(
			array(
				'conversation_id' => 1,
				'user_id' => 1,
				'body' => Crypt::encrypt('Hoi Mark'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			),
			array(
				'conversation_id' => 1,
				'user_id' => 2,
				'body' => Crypt::encrypt('Hallo Rick'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			),
			array(
				'conversation_id' => 1,
				'user_id' => 1,
				'body' => Crypt::encrypt('Hoe is het?'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			),
			array(
				'conversation_id' => 1,
				'user_id' => 2,
				'body' => Crypt::encrypt('Goed'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			),
			array(
				'conversation_id' => 2,
				'user_id' => 2,
				'body' => Crypt::encrypt('Hoi Carlo'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			),
			array(
				'conversation_id' => 2,
				'user_id' => 3,
				'body' => Crypt::encrypt('Hoi Mark, hoe is het ?'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			),
			array(
				'conversation_id' => 2,
				'user_id' => 2,
				'body' => Crypt::encrypt('Goed hoor, brb'),
				'created_at' => $this->randomDateTime(),
				'updated_at' => $this->randomDateTime()
			)
		);

		// Uncomment the below to run the seeder
		DB::table('messages')->insert($messages);
	}

	public function randomDateTime()
	{

		$datestart = strtotime('2010-01-01');//you can change it to your timestamp;
		$dateend = strtotime('2013-12-31');//you can change it to your timestamp;

		$daystep = 86400;

		$datebetween = abs(($dateend - $datestart) / $daystep);

		$randomday = rand(0, $datebetween);

		return date('Y-m-d H:i:s', $datestart + ($randomday * $daystep));

	}

}
