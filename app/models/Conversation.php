<?php

class Conversation extends BaseModel {
	protected $guarded = array();
	protected $fillable = array('user_id');

	public static $rules = array(
		'user_id' => 'required'
	);

	public function isParticipating()
	{

		$participants = $this->participants->lists('user_id');;
		$user_id = Auth::user()->getKey();

		$inArray = in_array($user_id, $participants);

		return $inArray;

	}

	public function participants(){
        return $this->hasMany('Participant');
    }

    public function messages(){
        return $this->hasMany('Message');
    }
}
