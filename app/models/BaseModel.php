<?php

class BaseModel extends Eloquent {

	public $errors;
	public $id;


	public static function boot()
	{
		parent::boot();

		static::saving(function($post) {
			
			$id = (isset($post['attributes']['id'])) ? $post['attributes']['id'] : null;

			return $post->validate($id);

		});

	}

	public function validate($id)
	{
		$validation = Validator::make($this->attributes, self::getValidationRules($id));

		if($validation->passes()) return true;

		$this->errors = $validation->messages();

		return false;
	}

	/**
     * Get validation rules and take care of own id on update
     * @param int $id
     * @return array
     */
    public static function getValidationRules($id = null)
    {

        $rules = static::$rules;
 
        if($id === null)
        {
            return $rules;
        }
 
        array_walk($rules, function(&$rules, $field) use ($id)
        {
            if(!is_array($rules))
            {
                $rules = explode("|", $rules);
            }
 
            foreach($rules as $ruleIdx => $rule)
            {

                // get name and parameters
                @list($name, $params) = explode(":", $rule);

                // only do someting for the unique rule
                if(strtolower($name) != "unique") {
                    continue; // continue in foreach loop, nothing left to do here
                }
 
                $p = array_map("trim", explode(",", $params));
 
                // set field name to rules key ($field) (laravel convention)
                if(!isset($p[1])) {
                    $p[1] = $field;
                }
 
                // set 3rd parameter to id given to getValidationRules()
                $p[2] = $id;
 
                $params = implode(",", $p);
                $rules[$ruleIdx] = $name.":".$params;
            }
        });
 
        return $rules;
    }

}