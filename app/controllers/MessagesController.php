<?php

class MessagesController extends BaseController {


	public function __construct(){

		$this->beforeFilter('auth', array());

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$input = Input::all();


		if( isset( $input['ids'] ) ){

			$ids = $input['ids'];
			$messages = Message::whereIn('id', $ids)->get();
		}else{
			$messages = Message::get();
		}

		foreach($messages as $message):
			$message->decrypt();
		endforeach;

		return array(
			'messages' => $messages->toArray()
		);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$message = Input::get('message');

		$data = array(
			'conversation_id' => $message['conversation_id'],
			'user_id' => Auth::user()->getKey()
		);

		// Set body if provided
		if($message['body'])
			$data['body'] = Crypt::encrypt($message['body']);

		$message = new Message($data);

		if($message->save()):

			$message->decrypt();

			return array(
				'message' => $message->toArray(),
				'user' => Auth::user()->toArray()
			);

		else:

			return $message->errors;

		endif;

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$message = Message::where('id', '=', $id)->first();
		$user = $message->user;

		if($message->count()):

			$message->decrypt();

			return array(
				'message' => $message->toArray()
			);

		else:

			$response = array(
				'message' => 'Not Found',
				'code'=> 404
			);

			return Response::json($response, 404);

		endif;

	}

}
