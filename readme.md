# Leren van framework Emberjs

In het werkveld van een front-end developer wisselen de tools en technieken zich in hoog tempo af. Wat vorig jaar relevant was kan op dit moment wel gedateerd zijn. Ik heb daarom ervoor gekozen om het framework EmberJS te gaan leren.

Vandaag de dag zijn er een tal van javascript frameworks te vinden. Een goed voorbeeld hiervan is <http://todomvc.com/>. Dit is een website waarbij dezelfde applicatie in alle frameworks wordt gemaakt. Hier zie je een tal van frameworks waaruit je kan kiezen. Enkele voorbeelden die mij al bekend waren zijn:

- Backbone.js
- AngularJS
- CanJS
- Knockout

Mijn plan hoe ik Ember ging leren was door het maken van een applicatie. Uiteraard heb ik ook enige video en blog-handleidingen gevolgd. De beste manier voor mij om nieuwe technieken te leren is door het zelf te doen. Op deze manier kom je problemen tegen die je niet tegen komt in tutorials. Ik heb om het te leren een berichtenapplicatie gemaakt.

Deze berichtenapplciatie heeft vergelijkbare functionaliteiten als iMessage, WhatsApp en Facebook messenger. Door deze proof-of-concept kwam ik snel achter wat prettig aan Ember was en wat niet. In dit hoofdstuk ga ik de voor en nadelen van Ember opnoemen en mijn eigen advies geadresseerd aan iemand die overweegt Ember te gebruiken.

## Voordelen

### Documentatie
De documentatie van EmberJS ziet er netjes en verzorgd uit <http://emberjs.com/api/>. Met veel voorbeelden en duidelijke handleiding kom je er snel uit om een simpele testapplicatie neer te zetten. Ook wat betreft leesbaarheid is de het goed opgebouwd. De meeste problemen die tegenkwam in mijn code kon ik met behulp van de documentatie oplossen.

### Live data
Wat voor mij erg handig was is hoe EmberJS omgaat met data. Wanneer je bepaalde properties in een model aanpast is dit direct terug te zien in de template op je webpagina. Hierdoor kan eenvoudig dynamische data tonen wat bij andere frameworks veel meer moeite kost.

### Automatische bestanden
EmberJS maakt automatisch Models, Controller en Views aan wanneer je deze niet zelf aanmaakt. Je hoeft om die reden dan ook niet onnodig veel bestanden aanmaken om je applicatie werkend te krijgen. Sommige andere frameworks lopen direct vast wanneer een Model, View of Controller niet is gedefineerd. Bij EmberJS gaat dit zogezegd automagisch.

### Support
Een belangrijk onderdeel van een framework is de support. Hoe beter de support hoe groter de kans is dat de applicatie sneller geüpdatet wordt. Bij EmberJS staat hier een indrukwekkende lijst. Hoewel er veel onbekende namen staan komen er een aantal mij wel bekend voor. Van Twitch, Groupon, Boxee en runtastic heb ik al eens eerder gehoord.


## Nadelen

### Leercurve
Naast alle positieve aspecten heeft EmberJS ook zijn nadelen. Zo duurt het lang voordat je het gevoel heb dat je het framework beheert. Zelfs dan loop je nog regelmatig tegen problemen aan. Andere frameworks als Backbone en Knockout zijn eenvoudiger om mee te beginnen. 

### Datakoppeling
Het eerste waar ik tegen aan liep was de koppeling met de data. Ik heb hier de RestAdapter voor gebruikt. Deze adapter vereiste dat mijn API op een bepaalde manier in elkaar moest zitten. De API die ik wilde gebruiken voldeed daar niet aan. Ik heb na veel onderzoek besloten zelf een API te gaan schrijven. Dit heb ik samen met mijn broer gedaan die meer back-end ervaring heeft. De API is gemaakt in het php-framework Laravel Het voordeel hiervan is dat je controle hebt over hoe je data naar Ember wordt verstuurd. 

### Historie
EmberJS is pad in augustus 2013 naar versie 1 gegaan. Ter vergelijking Backbone’s versie 1 kwam op Maart 2013 en Knockouts versie 1 kwam al in 2010. Nu zegt dit natuurlijk niet direct dat de een beter als de ander is. Maar de een is wel verder ontwikkeld. De meeste kinderziektes zullen bij een versie 3 er wel uit zijn. Hier moet je bij EmberJS nog lang op wachten. De adapter die zich met de data bezighoud is bij ember nog steeds in Beta, dit is nogal vervelend als je het voor productie wilt gebruiken.

### Niet kloppende documentatie
Hoewel de documentatie erg netjes eruit ziet zijn sommige onderdelen verouderd. Sommige links werken niet en wanneer ik iets letterlijk kopieerde en plakte gaf mijn applicatie een respons dat ik verouderde code gebruikte. Dit vond ik nogal vreemd. Ik verwacht van een documentatie dat het minstens up-to-date is.

## Conclusie
Ik heb een paar maanden mogen stoeien met EmberJS. Ik moet toegeven dat het wel potentie heeft. Ik zou het alleen zelf niet zo snel aan iemand aanbevelen. Als je echt een Ember-expert wilt worden zou je flink de tijd moeten nemen om alle onderdelen door te nemen. Want zoals eerder vermeld ligt de learning-curve nogal aan de hoge kant. Dat gezegd hebbende moet ik wel vermelden dat ik lang niet voldoende ervaring heb met javascript-frameworks om er een echt waardeoordeel over te geven. Een doorgewinterde javascript-guru zal er ongetwijfeld een andere mening op na houden. Je zult dit oordeel moeten zien vannuit het perspectief van een junior Front- end developer / student.



# De applicatie

### Algemeen
Omdat de API in laravel is geschreven staan alle client-side bestanden in de public folder. 
```
/public
```

Het index bestand en de handlebars templates staan op een andere plek

```
/app/views/pages/index.blade.php
/app/views/handlebar-templates/...
```

### Indeling
EmberJs werkt met outlets, de volgende outlets zijn gebruikt binnen de applicatie
```
{{outlet sidebar}}
{{outlet main}}

{{outlet login}}
```

De login wordt alleen voor het login formulier gebruikt.
De sidebar wordt gebruikt voor:
```
- Gesprekken
- Gebruikers
```

Kortom alle collecties die de applicatie heeft.
De main outlet wordt voor de detailweergaven gebruikt
Een specifiek gesprek of gebruiker in dit geval.


### Routes
De applicatie heeft de volgende routes:
```
applicationRoute

loginRoute

userRoute
usersRoute

conversationRoute
conversationsRoute
conversationsNewRoute
```

De application route zorgt ervoor dat de applicatie bij een error naar de loginpagina gaat.

De andere routes zorgen ervoor dat de juiste views in de juiste outlets geplaatst worden.


