App.ApplicationView = Ember.View.extend({
	controller: App.ApplicationController,
	init: function(){
		console.log("App.ApplicationView init")
		this._super();
		var self= this;

		$(window).on('resize', function(){
			self.onResize();
		});


	},

	onResize: function(){
		this.checkIfMobile();
	},

	checkIfMobile: function(){
		var windowWidth = $(window).width();

		if(windowWidth <= 640){
			Ember.set('App.mobile', true);
		}else{
			Ember.set('App.mobile', false);
		}
	}.on('init')



});