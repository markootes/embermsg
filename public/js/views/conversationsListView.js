App.ConversationsListView = Ember.CollectionView.extend({
  tagName: 'ul',
	classNames: ['conversationslist'],
	itemViewClass: App.ConversationsListitemView,
	controller: App.ConversationsController,
	templateName: 'conversationsList',

	setDeleteControlState: function(value){
		this.setEach('showDeleteControls', this.get('showDeleteControls'));
	}.observes('showDeleteControls'),

	emptyView: Ember.View.extend({
    	templateName: 'emptyConversationsListitem',
    	classNames: ['noconversation']
  }),


  hideLoader: function(){

      var self = this;

  		if(this.get('controller').get('requestComplete')){
  			this.get('controller').set('requestComplete', false);
  			this.iScroller.scrollTo(0,0, 500, IScroll.utils.ease.elastic);

        this.iScroller.on('scrollEnd', function(){
           self.iScrollInit();
        });
  		}

	}.observes('controller.requestComplete'),

  iScrollInit: function(){
    var self = this;

    this.iScroller = new IScroll('.conversationsscroller', {
      bounceEasing: 'elastic',
      useTransition: true,
      bounceTime: 1200,
      probeType:2,
      mouseWheel: true
    });

    this.iScroller.maxScrollY = 0;

    var canReload = true;

    this.iScroller.on('scroll', function () {

      if(Math.floor(this.y) == 0){
        canReload = true;
      }

      if(this.y > 70 && this.isInTransition == false && canReload){
        canReload = false;
        this.scrollTo(0,70);
        self.get('controller').send('reload');
        this.destroy();
      }
    });
  },

  didInsertElement: function(){
    this.iScrollInit();
  }

});