App.ConversationsListitemView = Ember.View.extend({
	tagName: 'li',
	templateName: 'conversationsListitem',
	classNameBindings: ['content.showDeleteControls:showdeleteconfirm','content.showDelete:showdelete'],
	controller: App.ConversationsController,

	init: function(){
		this._super();
		this.setIsMe();
	},


  	setIsMe: function(){
  		var self = this;
 		var participants = this.get('content.participants');


		participants.then(function(){
			var participantsIsMe = participants.filterBy('id', Ember.get('App.CurrentUser.id'));
			participantsIsMe.setEach('isMe', true);
		});

  	}.observes('App.CurrentUser'),

  	setTimeago: function(){

  		var self = this;

		jQuery.timeago.settings.strings = {
			prefixAgo: null,
			prefixFromNow: "",
			suffixAgo: "geleden",
			suffixFromNow: "van nu",
			seconds: "minder dan een minuut",
			minute: "ongeveer een minuut",
			minutes: "%d minuten",
			hour: "ongeveer een uur",
			hours: "ongeveer %d uur",
			day: "een dag",
			days: "%d dagen",
			month: "ongeveer een maand",
			months: "%d maanden",
			year: "ongeveer een jaar",
			years: "%d jaar",
			wordSeparator: " ",
			numbers: []
		};


		var latest_message = this.get('content.latest_message');

		latest_message.then(function(){

			var timestamp = $.timeago(latest_message.get('created_at'));
			self.set('content.latest_message.timeago', timestamp);

		});

    }.observes('content.latest_message')
});