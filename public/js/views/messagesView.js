App.MessagesView = Ember.CollectionView.extend({
	tagName: "ul",
	classNames: ['messageslist'],
	itemViewClass: App.MessageView
});