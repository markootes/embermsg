/* /router.js
*/

App.Router.map(function(){
  this.resource('users', function(){
  	this.resource('user', { path:'/:user_id' }, function(){
  	});
  });

  this.resource('conversations', function(){
  	this.route('new');
  	this.resource('conversation', { path:'/:conversation_id' }, function(){
  	});
  });

  this.route('login');
});