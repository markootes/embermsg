/* /routes/conversationsRoute.js */

App.ConversationRoute = Ember.Route.extend({


	model: function(params) {
		return this.store.find('conversation', params.conversation_id);
	},


	actions:{
		error: function(reason, transition){
			this.transitionTo('login');
		}
	},


	renderTemplate: function() {
		this.render({ outlet: 'main' });
	}
});