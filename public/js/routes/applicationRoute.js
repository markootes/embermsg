/* /routes/applicationRoute.js */

App.ApplicationRoute = Ember.Route.extend({
	actions:{
		error: function(){
			this.transitionTo('login');
		}
	}
});