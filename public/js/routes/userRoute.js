/* /routes/usersRoute.js
*/

App.UserRoute = Ember.Route.extend({
	model: function(params) {
		console.log(params);
		return this.store.find('user', params.user_id);
	},

  	renderTemplate: function() {
		this.render({ outlet: 'main'});
	}
});