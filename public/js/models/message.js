/* /models/user.js */
App.Message = DS.Model.extend({
	conversation_id: DS.attr("number"),
	user_id: DS.belongsTo('user', { async: true}),
	body: DS.attr("string"),
	created_at: DS.attr("string")
});
