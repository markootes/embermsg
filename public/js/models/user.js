/* /models/user.js */
App.User = DS.Model.extend({
	username: DS.attr(),
	email: DS.attr(),
	conversations: DS.hasMany('conversation', { async: true})
});
