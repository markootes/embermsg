App.ApplicationController = Ember.Controller.extend({

	init:function(){
		this.login();


		console.log(Ember.View.views);
	},

	isLoggedIn: function(){

		if(Ember.get('App.CurrentUser')){
			return true;
		}

		return false;

	}.property('App.CurrentUser'),

	login: function(){
		var self = this;

		var userPromise = Ember.$.getJSON('api/user');

		userPromise.then(function(response){
			// success
			Ember.set('App.CurrentUser', response.user);
			self.transitionToRoute('conversations');
		}, function(){
			// error
			Ember.set('App.CurrentUser', false);
			self.transitionTo('login');
		});


	}
});