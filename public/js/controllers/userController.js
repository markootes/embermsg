/* /controllers/userController.js
*/
App.UserController = Ember.ObjectController.extend({

	setConversationWithMe: function(){

		var self = this;
		var conversations = this.get('conversations');
		var currentUser = Ember.get('App.CurrentUser');

		conversations.then(function(){
			conversations.forEach(function(conversation, index, enumerable){
				var participants = conversation.get('participants');
				var isCurrentUserInConversation = participants.findBy('id', currentUser.id);

				if(isCurrentUserInConversation){
					conversation.set('conversationWithMe',true);
					self.set('hasConversationWithMe',true);
				}
			});
		})

	}.observes('conversations'),

	setIsMe: function(){
  		var self = this;

		if(Ember.get('App.CurrentUser.id') == this.get('id')){
			this.set('isMe', true);
		};

  	}.observes('App.CurrentUser'),


	actions:{
		backToUsers: function(){
			this.transitionToRoute('users');
		}
	}
});