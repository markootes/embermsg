App.LoginController = Ember.Controller.extend({
  needs: ['application'],

  loginFailed: false,
  isProcessing: false,

  actions: {

    login: function() {

      var AppController = this.get('controllers.application');

      this.setProperties({
        loginFailed: false,
        isProcessing: true
      });

      $.post("api/login", {
        email: this.get("email"),
        password: this.get("password")

      }).then(function(response) {

        Ember.set('App.CurrentUser', response);

        this.set("isProcessing", false);
        this.transitionToRoute('conversations');

      }.bind(this), function() {

        this.set("isProcessing", false);
        this.set("loginFailed", true);

      }.bind(this));
    }
  }

});