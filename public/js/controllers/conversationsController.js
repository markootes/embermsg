App.ConversationsController = Ember.ArrayController.extend({

    title: function(){
      return Ember.get('App.CurrentUser.username');
  	}.property('App.CurrentUser'),


    loadStatus: "Sleep naar beneden op te verversen",


    setLoadStatus: function(){
      var loading = this.get('loading');

      if(loading){
        this.set('loadStatus', "Gesprekken laden...");
      }else{
        this.set('loadStatus', "Sleep naar beneden om te verversen");
      }

    }.observes('loading'),

    setUpdatedAt: function(){
      var updatedAt = new Date();
          updatedAt = updatedAt.toLocaleTimeString('nl');
          updatedAt = "Laatst geupdatet om "+updatedAt;

          this.set("updatedAt", updatedAt);

    }.observes('content'),

  	actions: {


      showDelete: function(){
        this.set('showEditButton', true);
        this.get('content').setEach('showDeleteControls', true);
  		},

  		hideDelete: function(){
        this.set('showEditButton', false);
        this.get('content').setEach('showDeleteControls', false);
        this.get('content').setEach('showDelete', false);
  		},

      confirmDelete: function(conversation){
        this.get('content').setEach('showDelete', false);
        conversation.set('showDelete', true);
      },

      delete: function(conversation){

        var participants =  conversation.get('participants');
        var participants = participants.rejectBy('id', Ember.get('App.CurrentUser.id'));

        conversation.deleteRecord();
        conversation.save();

        participants.setEach('hasConversationWithMe',false);
      },


      reload: function() {
        var self = this;

        this.set('loading', true);

        var request = this.store.find('conversation');


        request.then(function(){

          setTimeout(function(){
            self.set("content", request);
            self.set('loading', false);
            self.set("requestComplete", true);

          }, 1000);



        });
      }


  	}


});